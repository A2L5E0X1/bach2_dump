FILES="vendor/firmware/isp_dts.img.gz"

if [[ "$(which gunzip)" = "" ]]; then
    echo "Missing dependencies: gunzip"
    exit 1
fi

for f in "$FILES"; do gunzip $f; done
